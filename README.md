### JMH Results ###

```
Benchmark                           Mode  Samples          Score  Score error  Units
c.a.MyBenchmark.baseline           thrpt      200  206025315.126  1702076.599  ops/s
c.a.MyBenchmark.plainRemap         thrpt      200   34430937.456   169578.337  ops/s
c.a.MyBenchmark.reflectionRemap    thrpt      200      46909.791      198.469  ops/s
```