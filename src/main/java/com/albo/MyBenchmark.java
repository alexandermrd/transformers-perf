/*
 * Copyright (c) 2005, 2014, Oracle and/or its affiliates. All rights reserved.
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS FILE HEADER.
 *
 * This code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 only, as
 * published by the Free Software Foundation.  Oracle designates this
 * particular file as subject to the "Classpath" exception as provided
 * by Oracle in the LICENSE file that accompanied this code.
 *
 * This code is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * version 2 for more details (a copy is included in the LICENSE file that
 * accompanied this code).
 *
 * You should have received a copy of the GNU General Public License version
 * 2 along with this work; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 * Please contact Oracle, 500 Oracle Parkway, Redwood Shores, CA 94065 USA
 * or visit www.oracle.com if you need additional information or have any
 * questions.
 */

package com.albo;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;
import org.apache.commons.beanutils.BeanUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;

public class MyBenchmark {
    private static HashMap<String, Object> params = new HashMap<String, Object>();
    static{
        params.put("someString", "alpaha");
        params.put("someDate", new Date());
        params.put("someLong", new Long(200L));
        params.put("someDecimal", BigDecimal.ONE);
    }

    @Benchmark    
    public AlboBean baseline() {
        AlboBean bean = new AlboBean();        
        return bean;
    }
    
    @Benchmark    
    public AlboBean plainRemap() {
        AlboBean bean = new AlboBean();        
        bean.setSomeString((String) params.get("someString"));
        bean.setSomeDate((Date) params.get("someDate"));
        bean.setSomeLong((Long) params.get("someLong"));
        bean.setBigDecimal((BigDecimal) params.get("someDecimal"));        
        return bean;
    }
    
    @Benchmark    
    public AlboBean reflectionRemap() throws IllegalAccessException, InvocationTargetException {
        AlboBean bean = new AlboBean();        
        BeanUtils.populate(bean, params);       
        return bean;
    }
    
    
    
    public static class AlboBean{
        String someString;
        Date someDate;
        Long someLong;
        BigDecimal bigDecimal;    

        public String getSomeString() {
            return someString;
        }

        public void setSomeString(String someString) {
            this.someString = someString;
        }

        public Date getSomeDate() {
            return someDate;
        }

        public void setSomeDate(Date someDate) {
            this.someDate = someDate;
        }

        public Long getSomeLong() {
            return someLong;
        }

        public void setSomeLong(Long someLong) {
            this.someLong = someLong;
        }

        public BigDecimal getBigDecimal() {
            return bigDecimal;
        }

        public void setBigDecimal(BigDecimal bigDecimal) {
            this.bigDecimal = bigDecimal;
        }
        
        
    }

}
